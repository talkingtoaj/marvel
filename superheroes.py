# Batman is a mix of both Human and Bat DNA
from humans import Human
from bats import Bat

class Batman(Human, Bat):

    # Batman has its own value for the species class attribute
    species = 'Superhero'

    def __init__(self, *args, **kwargs):
        # Typically to inherit attributes you have to call super:
        #super(Batman, self).__init__(*args, **kwargs)      
        # However we are dealing with multiple inheritance here, and super()
        # only works with the next base class in the MRO list.
        # So instead we explicitly call __init__ for all ancestors.
        # The use of *args and **kwargs allows for a clean way to pass arguments,
        # with each parent "peeling a layer of the onion".
        Human.__init__(self, 'anonymous', *args, **kwargs)
        Bat.__init__(self, *args, can_fly=False, **kwargs)
        # override the value for the name attribute
        self.name = 'Ben Affleck'

    def sing(self):
        return "nan nan nan nan nan I'm batman!"

